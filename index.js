const randomAnimalName = require("random-animal-name");

exports.handler = function (ctx, params) {
  ctx.logger.log("Hello from a JS function !")
  ctx.logger.log("I produce some logs just for development purposes...")

  let name = params["name"] || "world";
  return {
    message: `Hello ${name} !`,
    animalName: randomAnimalName()
  };
};
